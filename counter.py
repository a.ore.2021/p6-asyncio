import asyncio

async def counter(id, time):
    print(f"Counter {id}: starting...")
    for i in range(1, time + 1):
        print(f"Counter {id}: {i}")
        await asyncio.sleep(1)
    print(f"Counter {id}: finishing...")
async def main():
    await asyncio.gather(counter('A', 4), counter('B', 2), counter('C', 6))

asyncio.run(main())
